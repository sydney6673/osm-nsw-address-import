# Copy coordinates into a file called input
# $ split -l 100 input batch
# $ for file in batch*; do python import.py $file output-$file >> output-$file &; done
# $ cat output-* > final

import json
import sys
from urllib.parse import quote
from urllib.request import urlopen
from osgeo import ogr
from osgeo import osr

def get_file_total_lines(filename):
    with open(filename) as f:
        for i, l in enumerate(f):
            pass
    try:
        return i + 1
    except UnboundLocalError:
        return 0

total_processed_lines = get_file_total_lines(sys.argv[2])

if total_processed_lines == 0:
    print('"latitude";"longitude";"addr:housenumber";"addr:street"', flush=True)
else:
    total_processed_lines -= 1

with open(sys.argv[1]) as f:
    lines = f.readlines()

    for i, line in enumerate(lines):
        if i < total_processed_lines:
            continue

        lat, long = line.split(', ')

        source = osr.SpatialReference()
        source.ImportFromEPSG(4326)

        target = osr.SpatialReference()
        target.ImportFromEPSG(3857)

        transform = osr.CoordinateTransformation(source, target)

        point = ogr.CreateGeometryFromWkt('POINT ({} {})'.format(float(long), float(lat)))
        point.Transform(transform)

        x, y = point.ExportToWkt()[7:-1].split(' ')

        url = 'http://maps.six.nsw.gov.au/arcgis/rest/services/public/NSW_Property/MapServer/identify?geometry={}%2C{}&geometryType=esriGeometryPoint&sr=&layers=&layerDefs=&time=&layerTimeOptions=&tolerance=0&mapExtent=16818808.97403086%2C-4012764.233130625%2C16819095.61288684%2C-4012683.018788097&imageDisplay=600%2C550%2C96&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&dynamicLayers=&returnZ=false&returnM=false&gdbVersion=&f=pjson'.format(x, y)
        response = json.loads(urlopen(url).read().decode())

        for result in response['results']:
            data = result['attributes']['address'].split(' ')
            if data == ['Null']:
                continue
            break

        house_number = data.pop(0)
        suburb = data.pop()
        road_type = data.pop()
        road_name = ' '.join(data)

        # Edge cases discovered through experience.
        if road_name == 'THE':
            road_name = (road_name + ' ' + road_type).replace(' ', '%20')
        if road_type == 'BOULEVARDE':
            road_type = 'Bvd'

        try:
            road_name = quote(road_name)
            url = 'http://maps.six.nsw.gov.au/services/public/Address_Location?houseNumber={}&roadName={}&roadType={}&suburb={}&postCode=2121&projection=EPSG%3A4326'.format(house_number, road_name, road_type, suburb)
            response = json.loads(urlopen(url).read().decode())
            new_long = response['addressResult']['addresses'][0]['addressPoint']['centreX']
            new_lat = response['addressResult']['addresses'][0]['addressPoint']['centreY']
            road_name = response['addressResult']['addresses'][0]['roadName']
            road_type = response['addressResult']['addresses'][0]['roadType']

            # Some roads don't have a type, such as "The Boulevarde"
            if road_type == None:
                road_type = ''

            print('{};{};"{}";"{}"'.format(
                new_lat,
                new_long,
                house_number,
                (' '.join((road_name, road_type))).strip()
            ), flush=True)
        except Exception as e:
            print('ERROR: {};{};'.format(lat, long.rstrip()), flush=True)
